import { https } from "./configURL";

export const postSingin = (data) => {
  return https.post("/api/QuanLyNguoiDung/DangNhap", data);
};

export const postSingup = (data) => {
  return https.post(`/api/QuanLyNguoiDung/DangKy`, data);
};
