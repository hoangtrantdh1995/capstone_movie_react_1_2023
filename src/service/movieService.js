import { https } from "./configURL";

export const getMovieList = () => {
  return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP11`);
};

export const getMovieByTheater = () => {
  return https.get(`/api/QuanLyRap/LayThongTinLichChieuHeThongRap`);
};

export const getMovieInfo = (data) => {
  return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${data}`);
};

export const getMovieShowtime = (data) => {
  return https.get(`api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${data}`);
};

export const getMovieBanner = () => {
  return https.get(`/api/QuanLyPhim/LayDanhSachBanner`);
};

export const getMoviePagination = (data) => {
  return https.get(
    `/api/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=GP11&soTrang=${data}&soPhanTuTrenTrang=8`
  );
};
