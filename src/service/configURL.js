import axios from "axios";
import { store } from "../redux-toolkit/store";

const TOKEN_CYBERSOFT =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCAzNSIsIkhldEhhblN0cmluZyI6IjAzLzA2LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY4NTc1MDQwMDAwMCIsIm5iZiI6MTY1NzczMTYwMCwiZXhwIjoxNjg1ODk4MDAwfQ.KXn1XtehbphvfW3OSUFlLIzSrEtSLDtDQG4BgF38Cus";

const https = axios.create({
  baseURL: "https://movienew.cybersoft.edu.vn",
  headers: {
    TokenCybersoft: TOKEN_CYBERSOFT,
  },
});

setTimeout(() => {
  // store.subscribe((val) => {
  //   console.log("shfuhsdhf");
  // });
  const state = store.getState();
  const token = state.userSlice.user?.accessToken;
  https.interceptors.request.use((config) => {
    if (token) {
      config.headers["Authorization"] = "bearer " + token;
    }
    return config;
  });
});

export { https };
