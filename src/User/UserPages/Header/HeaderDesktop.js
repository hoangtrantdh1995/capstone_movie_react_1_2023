import React from "react";
import { NavLink } from "react-router-dom";
import Logo from "../../../assets/img_movie_CGV/cgvlogo.png";
import { useDispatch, useSelector } from "react-redux";
import { setUserInfo } from "../../../redux-toolkit/userSlice";

export default function Header() {
  let user = useSelector((state) => state.userSlice.user);
  const dispatch = useDispatch();

  const handleLogOut = () => {
    dispatch(setUserInfo(null));
  };

  const renderContent = () => {
    if (user) {
      return (
        <div className="flex justify-center items-center">
          <p className="font-semibold text-xl border-r-4 border-gray-500 pr-2">
            {" "}
            {user?.hoTen}
          </p>
          <button
            onClick={handleLogOut}
            className="self-center px-3 py-3 rounded text-xl flex justify-center hover:text-red-500"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-8 h-8 bg-slate-500 rounded-full text-white hover:bg-red-500"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75"
              />
            </svg>
            Đăng Xuất
          </button>
        </div>
      );
    } else {
      return (
        <div className="flex">
          <NavLink className={" hover:text-red-500"} to={"/sing-in"}>
            <button className="self-center px-3 py-3 rounded text-xl flex justify-center border-r-4 border-gray-500 pr-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 bg-slate-500 rounded-full text-white hover:bg-red-500"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                />
              </svg>
              Đăng nhập
            </button>
          </NavLink>

          <NavLink className={" hover:text-red-500"} to={"/sing-up"}>
            <button className="self-center px-3 py-3 rounded text-xl flex justify-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 bg-slate-500 rounded-full text-white hover:bg-red-500"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M19 7.5v3m0 0v3m0-3h3m-3 0h-3m-2.25-4.125a3.375 3.375 0 11-6.75 0 3.375 3.375 0 016.75 0zM4 19.235v-.11a6.375 6.375 0 0112.75 0v.109A12.318 12.318 0 0110.374 21c-2.331 0-4.512-.645-6.374-1.766z"
                />
              </svg>
              Đăng ký
            </button>
          </NavLink>
        </div>
      );
    }
  };

  return (
    <header className="p-2 bg-white opacity-90 dark:text-gray-100 fixed top-0 left-0 right-0 z-10">
      <div className="container flex justify-between h-16 mx-auto">
        <NavLink className={"px-16"} to={"/"}>
          <img src={Logo} alt="" />
        </NavLink>
        <ul className="items-stretch hidden space-x-3 text-black font-medium lg:flex">
          <li className="flex hover:text-red-500">
            <a
              rel="noopener noreferrer"
              href="#lichChieu"
              className="flex items-center px-4 -mb-1 border-b-2 dark:border-transparent"
            >
              Lịch Chiếu
            </a>
          </li>
          <li className="flex hover:text-red-500">
            <a
              rel="noopener noreferrer"
              href="#cumRap"
              className="flex items-center px-4 -mb-1 border-b-2 dark:border-transparent"
            >
              Cụm Rạp
            </a>
          </li>
          <li className="flex  hover:text-red-500">
            <a
              rel="noopener noreferrer"
              href="#tinTuc"
              className="flex items-center px-4 -mb-1 border-b-2 dark:border-transparent"
            >
              Tin Tức
            </a>
          </li>
          <li className="flex  hover:text-red-500">
            <a
              rel="noopener noreferrer"
              href="#ungDung"
              className="flex items-center px-4 -mb-1 border-b-2 dark:border-transparent"
            >
              Ứng dụng
            </a>
          </li>
        </ul>
        <div className="items-center flex-shrink-0 hidden font-semibold text-gray-500 lg:flex">
          {renderContent()}
        </div>
      </div>
    </header>
  );
}
