import React, { useState } from "react";
import { NavLink } from "react-router-dom";
import Logo from "../../../assets/img_movie_CGV/cgvlogo.png";
import { useDispatch, useSelector } from "react-redux";
import { setUserInfo } from "../../../redux-toolkit/userSlice";

export default function Header() {
  const [show, setShow] = useState(false);

  let user = useSelector((state) => state.userSlice.user);
  const dispatch = useDispatch();
  const handleLogOut = () => {
    setShow(!show);
    dispatch(setUserInfo(null));
  };

  const renderContent = () => {
    if (user) {
      return (
        <>
          <div className="flex items-center">
            <p className="font-semibold text-xl border-r-4 border-gray-500 pr-2">
              {" "}
              {user?.hoTen}
            </p>
            <button
              onClick={handleLogOut}
              className="self-center px-3 py-3 rounded text-xl flex justify-center hover:text-red-500"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 bg-slate-500 rounded-full text-white hover:bg-red-500"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15.75 9V5.25A2.25 2.25 0 0013.5 3h-6a2.25 2.25 0 00-2.25 2.25v13.5A2.25 2.25 0 007.5 21h6a2.25 2.25 0 002.25-2.25V15M12 9l-3 3m0 0l3 3m-3-3h12.75"
                />
              </svg>
              <div className="text-black px-3 font-semibold hover:text-red-500">
                Đăng xuất
              </div>
            </button>
          </div>
        </>
      );
    } else {
      return (
        <>
          <NavLink
            onClick={() => {
              setShow(!show);
            }}
            to={"/sing-in"}
          >
            <button className="ring-offset-4 self-center px-3 py-3 rounded text-xl flex justify-center border-gray-500 pr-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 bg-slate-500 rounded-full text-white hover:bg-red-500"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M17.982 18.725A7.488 7.488 0 0012 15.75a7.488 7.488 0 00-5.982 2.975m11.963 0a9 9 0 10-11.963 0m11.963 0A8.966 8.966 0 0112 21a8.966 8.966 0 01-5.982-2.275M15 9.75a3 3 0 11-6 0 3 3 0 016 0z"
                />
              </svg>
              <div className="text-black px-3 font-semibold hover:text-red-500">
                Đăng nhập
              </div>
            </button>
          </NavLink>

          <NavLink
            onClick={() => {
              setShow(!show);
            }}
            to={"/sing-up"}
          >
            <button className="self-center px-3 py-3 rounded text-xl flex justify-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 bg-slate-500 rounded-full text-white hover:bg-red-500"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M19 7.5v3m0 0v3m0-3h3m-3 0h-3m-2.25-4.125a3.375 3.375 0 11-6.75 0 3.375 3.375 0 016.75 0zM4 19.235v-.11a6.375 6.375 0 0112.75 0v.109A12.318 12.318 0 0110.374 21c-2.331 0-4.512-.645-6.374-1.766z"
                />
              </svg>
              <div className="text-black px-3 font-semibold hover:text-red-500">
                Đăng ký
              </div>
            </button>
          </NavLink>
        </>
      );
    }
  };

  return (
    <header className="p-2  overflow-hidde opacity-90 fixed top-0 left-0 right-10 z-10">
      <div className=" flex container justify-between h-16 mx-auto">
        <NavLink to={"/"}>
          <img src={Logo} alt="" />
        </NavLink>
        <button
          className="p-5  text-black bg-slate-300"
          onClick={() => {
            setShow(!show);
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-8 h-8"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
            />
          </svg>
        </button>
        {show ? (
          <div className="bg-blend-overlay fixed bg-black-rgba left-0 top-0 right-0 bottom-0">
            <div className="fixed top-0 right-0 bottom-0 w-80 bg-slate-100 max-w-full p-5">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-8 h-8 absolute text-gray-500 right-4"
                onClick={() => {
                  setShow(!show);
                }}
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
              <div className="mt-8">{renderContent()}</div>
              <div className="w-full h-px bg-red-600 mb-2 mt-3"></div>
              <div className=" leading-8">
                <ul>
                  <li
                    onClick={() => {
                      setShow(!show);
                    }}
                    className="text-gray-500 block no-underline text-lg leading-8 hover:text-red-500"
                  >
                    <NavLink to="/">Trang chủ</NavLink>
                  </li>
                  <li
                    onClick={() => {
                      setShow(!show);
                    }}
                    className="text-gray-500 block no-underline text-lg leading-8 hover:text-red-500"
                  >
                    <a href="#lichChieu"> Lịch chiếu</a>
                  </li>
                  <li
                    onClick={() => {
                      setShow(!show);
                    }}
                    className="text-gray-500 block no-underline text-lg leading-8 hover:text-red-500"
                  >
                    <a href="#cumRap">Cụm rạp</a>
                  </li>
                  <li
                    onClick={() => {
                      setShow(!show);
                    }}
                    className="text-gray-500 block no-underline text-lg leading-8 hover:text-red-500"
                  >
                    <a href="#tinTuc">Tin tức</a>
                  </li>
                  <li
                    onClick={() => {
                      setShow(!show);
                    }}
                    className="text-gray-500 block no-underline text-lg leading-8 hover:text-red-500"
                  >
                    <a href="#ungDung">Ứng dụng</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </header>
  );
}
