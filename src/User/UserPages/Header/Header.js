import React from "react";
import HeaderDesktop from "./HeaderDesktop";
import HeaderTablet from "./HeaderTablet";
import HeaderMobile from "./HeaderMobile";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";

export default function Header() {
  return (
    <div>
      <Desktop>
        <HeaderDesktop />
      </Desktop>
      <Tablet>
        <HeaderTablet />
      </Tablet>
      <Mobile>
        <HeaderMobile />
      </Mobile>
    </div>
  );
}
