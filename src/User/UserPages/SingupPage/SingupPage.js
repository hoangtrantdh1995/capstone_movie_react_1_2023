import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import SingupPageDesktop from "./SingupPageDesktop";
import SingupPageTablet from "./SingupPageTablet";
import SingupPageMobile from "./SingupPageMobile";

export default function SingupPage() {
  return (
    <div>
      <Desktop>
        <SingupPageDesktop />
      </Desktop>
      <Tablet>
        <SingupPageTablet />
      </Tablet>
      <Mobile>
        <SingupPageMobile />
      </Mobile>
    </div>
  );
}
