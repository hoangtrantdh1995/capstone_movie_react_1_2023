import React from "react";
import { Button, Checkbox, Form, Input, message, Select } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { postSingup } from "../../../service/userService";
import { UserAddOutlined } from "@ant-design/icons";
import bg_anime_1 from "../../../assets/lottie/sflf30_editor_n2fufki3.json";
import Lottie from "lottie-react";
import styles from "../../HOC/layout.module.css";

const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

export default function SingupPage() {
  const navigate = useNavigate();

  const onFinish = (values) => {
    console.log("values: ", values);
    postSingup(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng ký thành công");

        setTimeout(() => {
          navigate("/sing-in");
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng ký thất bại");
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="84">+84</Option>
      </Select>
    </Form.Item>
  );

  return (
    <div
      className={`${styles.bgLogin} flex items-center justify-center relative`}
    >
      <div className="bg-white flex w-2/3 absolute">
        <Lottie animationData={bg_anime_1} loop={true} className="w-2/5" />
        <div className="w-2/3 py-4">
          <div className="flex flex-col justify-center items-center mb-4">
            <UserAddOutlined className="text-yellow-50 text-3xl rounded-full w-14 h-14 flex justify-center items-center bg-blue-500" />
            <h2 className="text-red-500 font-bold">ĐĂNG KÝ</h2>
            <Form
              {...formItemLayout}
              layout="vertical"
              name="register"
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              initialValues={{
                residence: ["zhejiang", "hangzhou", "xihu"],
                prefix: "84",
              }}
              scrollToFirstError
              className="font-medium"
            >
              <Form.Item
                name="hoTen"
                tooltip="What do you want others to call you?"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập họ tên!",
                    whitespace: true,
                  },
                ]}
              >
                <Input placeholder="Họ tên" />
              </Form.Item>
              <Form.Item
                name="taiKhoan"
                tooltip="What do you want others to call you?"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tài khoản!",
                    whitespace: true,
                  },
                ]}
              >
                <Input placeholder="Tài khoản" />
              </Form.Item>

              <Form.Item
                name="email"
                rules={[
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                  {
                    required: true,
                    message: "Please input your E-mail!",
                  },
                ]}
              >
                <Input placeholder="E-mail" />
              </Form.Item>

              <Form.Item
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mật khẩu!",
                  },
                ]}
                hasFeedback
              >
                <Input.Password placeholder="Mật khẩu" />
              </Form.Item>

              <Form.Item
                name="matKhau"
                dependencies={["password"]}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: "Nhập lại mật khẩu!",
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue("matKhau") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error("Mật khẩu không khớp!"));
                    },
                  }),
                ]}
              >
                <Input.Password placeholder="Nhập lại mật khẩu" />
              </Form.Item>

              <Form.Item
                name="soDt"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập số điện thoại!",
                  },
                ]}
              >
                <Input
                  placeholder="Số điện thoại"
                  addonBefore={prefixSelector}
                  style={{
                    width: "100%",
                  }}
                />
              </Form.Item>

              <Form.Item
                name="agreement"
                valuePropName="checked"
                rules={[
                  {
                    validator: (_, value) =>
                      value
                        ? Promise.resolve()
                        : Promise.reject(new Error("Should accept agreement")),
                  },
                ]}
                {...tailFormItemLayout}
                wrapperCol={{
                  span: 16,
                }}
              >
                <Checkbox name="agreement">
                  Tôi đồng ý{" "}
                  <a href="" className="text-blue-600">
                    điều kiện thỏa thuận
                  </a>
                </Checkbox>
              </Form.Item>

              <Form.Item className="text-center">
                <Button className="bg-blue-500 text-white" htmlType="submit">
                  Đăng ký
                </Button>
              </Form.Item>
            </Form>
            <div className="text-lg font-medium text-purple-800">
              Bạn đã có tài khoản?
              <NavLink
                to="/sing-in"
                className="pl-3 border-b-2 text-xl border-red-500 hover:text-red-500"
              >
                Đăng nhập
              </NavLink>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
