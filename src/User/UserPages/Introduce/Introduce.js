import React from "react";
import styles from "../../HOC/layout.module.css";
import Apple from "../../../assets/img_movie_CGV/apple.png";
import Android from "../../../assets/img_movie_CGV/android.png";

export default function Introduce() {
  return (
    <div id="ungDung" className="pb-10">
      <div className={`${styles.bgIntro} p-16`}>
        <div className="text-left text-white">
          <h1 className="text-3xl pb-10">
            Ứng dụng tiện lợi dành cho người yêu điện ảnh
          </h1>
          <p className="pb-10 text-xl">
            Không chỉ đặt vé, bạn còn có thể bình luận phim,
            <br /> chấm điểm rạp và đổi quà hấp dẫn.
          </p>
          <button className="bg-red-500 py-5 rounded hover:bg-red-600">
            APP MIỄN PHÍ - TẢI VỀ NGAY!
          </button>
          <div className="space-y-3 py-10">
            <h3 className="uppercase dark:text-gray-50 font-semibold">
              MOBILE APP
            </h3>
            <ul className="space-y-1 flex gap-4 items-center ">
              <li>
                <a
                  rel="noopener noreferrer"
                  href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197"
                >
                  <img className="w-8 h-8" src={Apple} alt="" />
                </a>
              </li>
              <li>
                <a
                  rel="noopener noreferrer"
                  href="https://apps.apple.com/vn/app/tix-%C4%91%E1%BA%B7t-v%C3%A9-nhanh-nh%E1%BA%A5t/id615186197"
                >
                  <img className="w-8 h-8" src={Android} alt="" />
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
