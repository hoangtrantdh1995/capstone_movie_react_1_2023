import React from "react";
import { useDispatch } from "react-redux";
import { NavLink, useNavigate } from "react-router-dom";
import { Button, Form, Input, message, Checkbox } from "antd";
import { postSingin } from "../../../service/userService";
import { setUserInfo } from "../../../redux-toolkit/userSlice";
import { UserOutlined } from "@ant-design/icons";
import bg_anime_1 from "../../../assets/lottie/sflf30_editor_n2fufki3.json";
import Lottie from "lottie-react";
import styles from "../../HOC/layout.module.css";

export default function SinginPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onFinish = (values) => {
    postSingin(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành công");
        dispatch(setUserInfo(res.data.content));

        setTimeout(() => {
          navigate("/");
        });
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng nhập thất bại");
        setTimeout(() => {
          navigate("/sing-up");
        }, 1000);
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div
      className={`${styles.bgLogin} flex items-center justify-center relative`}
    >
      <div className="bg-white flex w-full py-8 absolute">
        <Lottie animationData={bg_anime_1} loop={true} className="w-2/5" />
        <div className="w-3/5">
          <div className="flex flex-col justify-center items-center mb-4">
            <UserOutlined className="text-yellow-50 text-3xl rounded-full w-14 h-14 flex justify-center items-center bg-blue-500" />
            <h2 className="text-red-500 font-bold">ĐĂNG NHẬP</h2>
          </div>
          <Form
            layout="vertical"
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item
              name="taiKhoan"
              rules={[{ required: true, message: "Vui lòng nhập tài khoản!" }]}
            >
              <Input placeholder="Tài khoản" />
            </Form.Item>

            <Form.Item
              name="matKhau"
              rules={[{ required: true, message: "Vui lòng nhập mật khẩu!" }]}
            >
              <Input.Password placeholder="Mật khẩu" />
            </Form.Item>

            <Form.Item name="remember" valuePropName="checked">
              <Checkbox>Nhớ tài khoản</Checkbox>
            </Form.Item>

            <Form.Item className="text-center">
              <Button className="bg-blue-500 text-white" htmlType="submit">
                Đăng nhập
              </Button>
            </Form.Item>
          </Form>
          <div className="text-lg font-medium text-purple-800">
            Bạn chưa có tài khoản?
            <NavLink
              to="/sing-up"
              className="pl-3 border-b-2 text-xl border-red-500 hover:text-red-500"
            >
              Đăng ký
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  );
}
