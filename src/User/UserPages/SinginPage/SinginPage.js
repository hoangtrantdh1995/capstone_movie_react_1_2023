import React from "react";
import { Desktop, Mobile, Tablet } from "../../HOC/Responsive";
import SinginPageDesktop from "./SinginPageDesktop";
import SinginPageTablet from "./SinginPageTablet";
import SinginPageMobile from "./SinginPageMobile";

export default function SinginPage() {
  return (
    <div>
      <Desktop>
        <SinginPageDesktop />
      </Desktop>
      <Tablet>
        <SinginPageTablet />
      </Tablet>
      <Mobile>
        <SinginPageMobile />
      </Mobile>
    </div>
  );
}
