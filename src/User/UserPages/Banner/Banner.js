import React, { useEffect, useState } from "react";
import { Carousel } from "antd";
import { getMovieBanner } from "../../../service/movieService";
const contentStyle = {
  height: "600px",
  width: "100%",
  lineHeight: "600px",
  backgroundPosition: "top center",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
};

export default function Banner() {
  const [movieBaner, setMovieBanner] = useState([]);

  useEffect(() => {
    getMovieBanner()
      .then((res) => {
        console.log(res);
        setMovieBanner(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderMovieBanner = () =>
    movieBaner.map((item, index) => {
      return (
        <div key={index}>
          <div
            style={{
              ...contentStyle,
              backgroundImage: `url(${item.hinhAnh})`,
            }}
          ></div>
        </div>
      );
    });

  return (
    <div className="w-screen">
      <Carousel autoplay>{renderMovieBanner()}</Carousel>
    </div>
  );
}
