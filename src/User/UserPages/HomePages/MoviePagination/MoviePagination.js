import React from "react";
import { Desktop, Mobile, Tablet } from "../../../HOC/Responsive";
import MoviePaginationDesktop from "./MoviePaginationDesktop";
import MoviePaginationTablet from "./MoviePaginationTablet";
import MoviePaginationMobile from "./MoviePaginationMobile";

export default function MoviePagination() {
  return (
    <div>
      <Desktop>
        <MoviePaginationDesktop />
      </Desktop>
      <Tablet>
        <MoviePaginationTablet />
      </Tablet>
      <Mobile>
        <MoviePaginationMobile />
      </Mobile>
    </div>
  );
}
