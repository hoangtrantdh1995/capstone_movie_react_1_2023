import React, { useEffect, useState } from "react";
import { Pagination } from "antd";
import { getMoviePagination } from "../../../../service/movieService";
import { NavLink } from "react-router-dom";
import { Card } from "antd";

const { Meta } = Card;

export default function MoviePagination() {
  const [moviePagination, setMoviePagination] = useState();
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    getMoviePagination(currentPage)
      .then((res) => {
        console.log(res);
        setMoviePagination(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [currentPage]);

  const renderMovieList = () =>
    moviePagination?.items.map((item, index) => {
      return (
        <Card
          style={{
            width: 200,
          }}
          key={index}
          cover={
            <img
              alt="example"
              src={item.hinhAnh}
              className="h-60 object-cover"
            />
          }
        >
          <Meta
            title={<h2 className="text-blue-500 h10">{item.tenPhim}</h2>}
            description={
              <h4 className="h-16">
                {item.moTa.length < 50
                  ? item.moTa
                  : item.moTa.slice(0, 30) + "..."}
              </h4>
            }
          />
          <NavLink
            to={`/detail/${item.maPhim}`}
            className="bg-red-500 px-5 py-2 text-white rounded"
          >
            Xem chi tiết
          </NavLink>
        </Card>
      );
    });

  const onChange = (page) => {
    console.log(page);
    setCurrentPage(page);
  };

  return (
    <div id="lichChieu" className="py-10 px-30 container mx-auto flex-wrap">
      <div className="grid grid-cols-2 md:grid-cols-3 text-center gap-3">
        {renderMovieList()}
      </div>
      <Pagination
        className="text-center py-3"
        total={moviePagination?.totalCount}
        onChange={onChange}
        current={currentPage}
      />
    </div>
  );
}
