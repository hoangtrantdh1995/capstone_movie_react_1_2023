import React from "react";
import Banner from "../../Banner/Banner";
import Introduce from "../../Introduce/Introduce";
import MoviePagination from "../MoviePagination/MoviePagination";
import MovieTab from "../MovieTab/MovieTab";

export default function HomePage() {
  return (
    <div className="pt-24">
      <Banner />
      <MoviePagination />
      <MovieTab />
      <Introduce />
    </div>
  );
}
