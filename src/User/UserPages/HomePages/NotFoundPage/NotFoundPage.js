import React from "react";

export default function NotFoundPage() {
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <h1 className="text-center text-red-600 text-9xl font-black animate-bounce">
        404 Page
      </h1>
      <span class="flex h-3 w-3">
        <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-sky-400 bg-opacity-95"></span>
        <span class="relative inline-flex rounded-full h-auto w-auto"></span>
      </span>
    </div>
  );
}
