import React from "react";
import dayjs from "dayjs";
dayjs.locale("vi");

export default function MoviItemTab({ movie }) {
  return (
    <div className="flex mt-40 space-x-6">
      <img className="h-40 w-28 object-cover" src={movie.hinhAnh} alt="" />
      <div>
        <h3 className="font-medium text-2xl">{movie.tenPhim}</h3>
        <div className="grid grid-cols-3 gap-7">
          {movie.lstLichChieuTheoPhim.slice(0, 6).map((lichChieu, index) => (
            <p
              className="bg-amber-600 text-white px-3 py-2 rounded"
              key={index}
            >
              {dayjs(lichChieu.ngayChieuGioChieu).format("DD/MM/YYYY  hh:mm A")}
            </p>
          ))}
        </div>
      </div>
    </div>
  );
}
