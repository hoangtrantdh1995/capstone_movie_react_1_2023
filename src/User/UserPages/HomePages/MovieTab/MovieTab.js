import React, { useEffect, useState } from "react";
import { getMovieByTheater } from "../../../../service/movieService";
import { Tabs } from "antd";
import MoviItemTab from "../MovieItemTab/MoviItemTab";

export default function MovieTab() {
  const [dataMovie, setDataMovie] = useState([]);

  useEffect(() => {
    getMovieByTheater()
      .then((res) => {
        console.log(res);
        setDataMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const onChange = (key) => {
    console.log(key);
  };

  const renderMovieListByTheaterCluster = (tc) => (
    <div>
      {tc.danhSachPhim.map((movie, index) => {
        return (
          <div>
            <MoviItemTab movie={movie} key={index} />
          </div>
        );
      })}
    </div>
  );

  const renderTheTheaterCluster = (ts) =>
    ts.lstCumRap.map((tc, index) => {
      return {
        label: (
          <div className="w-3/12" key={index}>
            <h4 className="text-xl font-medium">{tc.tenCumRap} </h4>
            <p className="hover:text-black">{tc.diaChi}</p>
          </div>
        ),
        key: tc.maCumRap,
        children: (
          <div style={{ height: 540, overflowY: "scroll" }}>
            {renderMovieListByTheaterCluster(tc)}
          </div>
        ),
      };
    });

  const renderTheaterSystem = () =>
    dataMovie.map((ts, index) => {
      return {
        label: (
          <div className="">
            <img className="w-14 h-14" src={ts.logo} alt="" />
          </div>
        ),
        key: ts.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: 540,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderTheTheaterCluster(ts)}
          />
        ),
      };
    });

  return (
    <div id="cumRap" className="container mx-auto pb-12">
      <Tabs
        style={{
          height: 540,
        }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderTheaterSystem()}
      />
    </div>
  );
}
