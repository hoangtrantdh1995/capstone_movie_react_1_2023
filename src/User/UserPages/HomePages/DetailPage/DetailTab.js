import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getMovieShowtime } from "../../../../service/movieService";
import { Tabs } from "antd";
import DetailItemtab from "./DetailItemtab";

const onChange = (key) => {
  console.log(key);
};

export default function DetailTab() {
  const param = useParams();
  const [movieShowtime, setMovieShowtime] = useState();

  useEffect(() => {
    getMovieShowtime(param.id)
      .then((res) => {
        console.log(res);
        setMovieShowtime(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [param.id]);

  const renderMovieListByTheaterClusterDetail = (tc) => (
    <div>
      {tc.lichChieuPhim?.map((movie, index) => {
        return (
          <div>
            <DetailItemtab movie={movie} key={index} />
          </div>
        );
      })}
    </div>
  );

  const renderTheTheaterClusterDetail = (ts) =>
    ts.cumRapChieu?.map((tc, index) => {
      return {
        label: (
          <div className="w-3/12" key={index}>
            <h4>{tc.tenCumRap} </h4>
            <p>{tc.diaChi}</p>
          </div>
        ),
        key: tc.maCumRap,
        children: (
          <div style={{ height: 450, overflowY: "scroll" }}>
            {renderMovieListByTheaterClusterDetail(tc)}
          </div>
        ),
      };
    });

  const renderTheaterSystemDetail = () =>
    movieShowtime?.heThongRapChieu.map((ts, index) => {
      return {
        label: (
          <div className="">
            <img className="w-14 h-14" src={ts.logo} alt="" />
          </div>
        ),
        key: ts.maHeThongRap,
        children: (
          <Tabs
            style={{
              height: 450,
            }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
            items={renderTheTheaterClusterDetail(ts)}
          />
        ),
      };
    });

  return (
    <div className="container mx-auto pb-12">
      <Tabs
        style={{
          height: 450,
        }}
        tabPosition="left"
        defaultActiveKey="1"
        onChange={onChange}
        items={renderTheaterSystemDetail()}
      />
    </div>
  );
}
