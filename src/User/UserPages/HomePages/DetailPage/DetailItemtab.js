import React from "react";
import dayjs from "dayjs";
dayjs.locale("vi");

export default function DetailItemtab({ movie }) {
  return (
    <div className="flex mt-40 space-x-6">
      <div>
        <div className="">
          <p className="bg-amber-600 text-white px-2 py-2 rounded">
            {dayjs(movie.ngayChieuGioChieu).format("DD/MM/YYYY  hh:mm A")}
          </p>
        </div>
      </div>
    </div>
  );
}
