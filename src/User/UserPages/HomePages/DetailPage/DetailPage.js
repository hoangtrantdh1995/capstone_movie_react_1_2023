import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { getMovieInfo } from "../../../../service/movieService";
import { Modal } from "antd";
import DetailTab from "./DetailTab";
import { StarFilled } from "@ant-design/icons";
import dayjs from "dayjs";
dayjs.locale("vi");

export default function DetailPage() {
  let params = useParams();
  const [movieInfo, setMovieInfo] = useState();

  useEffect(() => {
    getMovieInfo(params.id)
      .then((res) => {
        console.log(res);
        setMovieInfo(res.data.content);
        console.log("res.data.content: ", res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [params.id]);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const renderMovieInfo = () => {
    return (
      <>
        <div className="bg-slate-800">
          <div className="py-24 grid grid-cols-4 items-center">
            <img
              className="w-80 h-auto "
              src={movieInfo?.hinhAnh}
              onClick={showModal}
              alt=""
            />
            <div className="text-white pl-10">
              <p className="font-semibold text-2xl hover:text-red-300">
                {movieInfo?.tenPhim}
              </p>
              <p className="px-2 py-2 text-lg">
                {dayjs(movieInfo?.ngayChieuGioChieu).format("DD/MM/YYYY")}
              </p>
              <p className="pb-9">120 phút</p>
              <button
                onClick={showModal}
                className="bg-red-400 px-3 rounded text-center font-medium text-2xl hover:bg-red-500"
              >
                Xem Trailer
              </button>
            </div>
            <div className="flex items-end text-yellow-500">
              <StarFilled />
              <StarFilled />
              <StarFilled />
              <StarFilled />
              <StarFilled />
            </div>
          </div>
          <Modal
            title={`Trailer - ${movieInfo?.tenPhim}`}
            open={isModalOpen}
            onOk={handleOk}
            onCancel={handleCancel}
            destroyOnClose={true}
            okType={"default"}
          >
            <div
              className="embed-responsive embed-responsive-21by9 relative w-full overflow-hidden"
              style={{
                paddingTop: "42.857143%",
                height: "400px",
              }}
            >
              <iframe
                className="embed-responsive-item absolute top-0 right-0 bottom-0 left-0 w-full h-full"
                src={movieInfo?.trailer}
                allowFullScreen
                data-gtm-yt-inspected-2340190_699="true"
                id={240632615}
              />
            </div>
          </Modal>
        </div>
      </>
    );
  };

  return (
    <div className="container mx-auto text-center">
      {renderMovieInfo()}
      <DetailTab />
    </div>
  );
}
