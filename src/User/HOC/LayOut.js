import React from "react";
import Footer from "../UserPages/Footer/Footer";
import Header from "../UserPages/Header/Header";

export default function LayOut({ children }) {
  return (
    <div>
      <Header />
      {children}
      <Footer />
    </div>
  );
}
