import { BrowserRouter, Route, Routes } from "react-router-dom";
import LayOut from "./User/HOC/LayOut";
import DetailPage from "./User/UserPages/HomePages/DetailPage/DetailPage";
import HomePage from "./User/UserPages/HomePages/HomePage/HomePage";
import NotFoundPage from "./User/UserPages/HomePages/NotFoundPage/NotFoundPage";
import SinginPage from "./User/UserPages/SinginPage/SinginPage";
import SingupPage from "./User/UserPages/SingupPage/SingupPage";
import "./App.css";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <LayOut>
                <HomePage />
              </LayOut>
            }
          />
          <Route
            path="/sing-in"
            element={
              <LayOut>
                <SinginPage />
              </LayOut>
            }
          />
          <Route
            path="/sing-up"
            element={
              <LayOut>
                <SingupPage />
              </LayOut>
            }
          />
          <Route
            path="/detail/:id"
            element={
              <LayOut>
                <DetailPage />
              </LayOut>
            }
          />
          <Route path="*" element={<NotFoundPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
